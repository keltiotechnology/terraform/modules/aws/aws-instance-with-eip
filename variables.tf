variable "env" {
  type = string
}

variable "ami" {
  type = string
  # Ubuntu Server 16.04 LTS (HVM), SSD Volume Type
  default = "ami-af79ebc0"
}

variable "instance_type" {
  type    = string
  default = "t2.nano"
}

variable "vpc_security_group_id" {
  type = string
}

variable "subnet_id" {
  type = string
}

variable "deployer_key_name" {
  type = string
}
