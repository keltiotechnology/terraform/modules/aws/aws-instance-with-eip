//////////////////////////////////////////////////////////////////////
////
////   INSTANCES CONFIGS
////
///////////////////////////////////////////////////////////////////////

resource "aws_instance" "web_instance" {
  ami                         = var.ami
  instance_type               = var.instance_type
  key_name                    = var.deployer_key_name
  associate_public_ip_address = true

  tags = {
    Name = "${var.env}-monamigeek-vm"
  }

  vpc_security_group_ids = [
    "${var.vpc_security_group_id}"
  ]
  subnet_id  = var.subnet_id
  monitoring = true

  root_block_device {
    volume_size = 100
  }
}

resource "aws_eip" "web_instance_floating_ip" {
  vpc      = true
  instance = aws_instance.web_instance.id
}
